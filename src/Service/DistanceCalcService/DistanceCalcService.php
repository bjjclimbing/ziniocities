<?php
namespace Service\DistanceCalcService;
use \count as count;
/**
 * DistanceCalc
 * 
 * Contains functions needed to calculate the distance between cities 
 * and configure the most optimal route from a given list of destinations
 */
class DistanceCalcService {

    const EARTH_RAD = 6371;

    /**
     * Calculates the distance between two cities based on the haversina 
     * formula
     * @param array $cityA
     * @param array $cityB
     * @return int
     */
    private function getDistance(array $cityA, array $cityB) {
        $latDif = $this->deg2rad($cityB['lat'] - $cityA['lat']);
        $lonDif = $this->deg2rad($cityB['lon'] - $cityA['lon']);
        $lata=$this->deg2rad($cityA['lat']);
        $latb=$this->deg2rad($cityB['lat']);
        $haversina = sin($latDif / 2) * sin($latDif / 2) +
                sin($lonDif/2)*sin($lonDif/2)*cos($lata)*cos($latb);
        $c = 2 * atan2(sqrt($haversina), sqrt(1 - $haversina));
        $distance = self::EARTH_RAD * $c;
        return $distance;
    }

    /**
     * Converts from degrees to radians
     * @param float $deg
     * @return float
     */
    private function deg2rad(float $deg) {
        return $deg * (pi() / 180);
    }
    
    /**
     * Calculates the most cost efficient route to cover a list of cities
     * @param array $destinationList
     * @return array
     */
    public function getRoute( $destinationList) {
        if(!is_array($destinationList))
        {
            throw new \Exception('Must Suply list of cities',906);
        }
        $baseCity = $destinationList[0];
        $currentChamp = $destinationList[1];
        for ($fp = 2; $fp <= count($destinationList) - 1; $fp++) {
            $champIndex = $fp - 1;
            for ($index = $fp; $index <= count($destinationList) - 1; $index++) {
                $distanceA = $this->getDistance($baseCity, $currentChamp);
                $distanceB = $this->getDistance($baseCity, $destinationList[$index]);
                if ($distanceA < $distanceB) {
                    $currentChamp = $currentChamp;
                } else {
                    $temfile = $destinationList[$champIndex];
                    $currentChamp = $destinationList[$index];
                    $destinationList[$champIndex] = $destinationList[$index];
                    $destinationList[$index] = $temfile;
                }
            }
            $baseCity = $currentChamp;
            $currentChamp = $destinationList[$fp];
        }
        return $destinationList;
    }

}
