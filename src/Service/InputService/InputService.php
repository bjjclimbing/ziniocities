<?php
namespace Service\InputService;
use Service\InputService\Model\InputInterface;
use Service\InputService\Model\InputFile;
/**
 * The input Service makes use of the input interface in order to allow changes on 
 * the application behavior
 */
class InputService
{
    protected $parameters;
            
    private function initService(){
        require __DIR__.'/../../../Config/config.php';
        $this->parameters=array(
            'inputDataFormat'=>$inputDataFormat,
            'fileName'=> $file,
            'baseCity'=>            $baseCity,
            'fileSeparator'=>$fileseparator,
            'itemsPerLines'=>$itemsPerLine,
        );
    }
    public function getDataCities(){
        $this->initService();
        switch($this->parameters['inputDataFormat']){
            case InputInterface::FILE:
                $inputFile= new InputFile($this->parameters);
                $cities=$inputFile->returnData();
                break;
        }
        return $cities;
    }
}