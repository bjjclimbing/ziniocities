<?php

namespace Service\InputService\Model;
use Service\InputService\Model\InputInterface;
/**
 * Extends from InputInterface and aplies the established methods 
 * to data aquired from a text file
 *
 * @author luis
 */
class InputFile implements InputInterface {
    private $inputFile;
    private $separator;
    private $basecity;
    private $itemsPerLine;
    public function __construct(array $parameters) {
        $this->inputFile=$parameters[InputInterface::INPUT_FILENAME];
        $this->separator=$parameters[InputInterface::INPUT_SEPARATOR];
        $this->basecity=$parameters[InputInterface::BASE_CITY];
        $this->itemsPerLine=$parameters[InputInterface::ITEMS];
    }
    private function orderData(array $cities) {
        $baseCity = $this->basecity;
        $found=false;
        if ($baseCity) {

            if ($baseCity === $cities[0]) {
                $found=true;
                return $cities;
            }
            for ($index = 0; $index <= count($cities) - 1; $index++) {
                if ($cities[$index]['city'] === $baseCity) {
                    $temp = $cities[0];
                    $cities[0] = $cities[$index];
                    $cities[$index] = $temp;
                    $found=true;
                }
            }
            if($found){
                return $cities;
            }
        }
        throw new \Exception('City does not exists in file',904);
    }

    private function readData() {
        if ( !file_exists($this->inputFile) ) {
            throw new \Exception('',901);
      }
        $handler = fopen($this->inputFile, 'r');
        if ($handler) {
            while (($line = fgets($handler)) !== false) {
                if( !preg_match("/^\/.+\/[a-z]*$/i",$this->separator)){
                    throw new \Exception('',905);
                }
                $cityConf = preg_split($this->separator, $line);
                if(count($cityConf)!=$this->itemsPerLine){
                    throw new \Exception('',905);
                }
                $city = $cityConf[0] ?? null;
                $lat = floatval($cityConf[1]) ?? null;
                $lon = floatval($cityConf[2]) ?? null;
                $cityBundle[] = array('city' => $city, 'lat' => $lat, 'lon' => $lon);
            }
            fclose($handler);

            return $cityBundle;
        }
        throw new \Exception('',902);
        //return 902;
    }

    public function returnData() {
        $cities=$this->readData();
        if(!is_array($cities)){
            $errCode=$cities;
            return $errCode;
        }
        $orderedCities=$this->orderData($cities);
        if(!is_array($orderedCities)){
            $errCode=$orderedCities;
            return $errCode;
        }
        return $orderedCities;
    }

}
