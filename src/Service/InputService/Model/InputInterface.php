<?php

namespace Service\InputService\Model;


/**
 * Defines the methods which every class that implements it should have
 * this interface abstracts the way the application is configured allowing
 * the administrators to change it's configuration parameters 
 *
 * @author luis
 */
interface InputInterface {

    
    const FILE = 'file';
    const INPUT_SEPARATOR = 'fileSeparator';
    const INPUT_FILENAME = 'fileName';
    const BASE_CITY = 'baseCity';
    const ITEMS = 'itemsPerLines';

    public function returnData();
}
