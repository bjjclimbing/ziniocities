<?php
    require_once __DIR__.'/../vendor/autoload.php';
    function my_autoload ($pClassName) {
        $pClassName=str_replace('\\' ,'/',$pClassName);
        include(__DIR__ . "/" . $pClassName . ".php");
    }
    
    spl_autoload_register("my_autoload");
    