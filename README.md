Zinio Cities

Calculates the most cost efficient roadmap to travel through a list of cities and their geographical coordinates. 

The technique used to measure the distance between the cities is the haversine algorithm which uses the center point of the earth and the given coordinates to create a triangle whose base length is the shortest straight distance between both coordinates

The script requires an input file with the list of the cities and its latitude and longitude coordinates . 

Installation

Once the project is cloned, it is required to use composer to install phpunit and include the projects classes and it's namespaces

composer install

Configuration

The main configuration file is located on 
Config/config.php

$inputDataFormat='file';
$file = __DIR__ . "/../cities.txt"; 
$baseCity = 'Beijing';
$fileseparator = "/[\t]/"; 
$itemsPerLine=3;

The configuration shown above states the following

$inputDataFormat  ->    specifies the type or format the data will be provided (currently file is the only supported type)
$file -> The full path to the file containing the list of cities
$baseCity -> states the name of the city from the list that will be used as the home city to start the route
$fileseparator -> regular expression that defines the way the data is separated in the file (tab)
$itemsPerLine -> establishes the fixed amount of items listed on every line of the file

Use

Once the configuration is set, it is necessary to provide the list of cities located as stated on the config file. Once the app is configured the script may run

php solve.php

Expansion

The application may be extended to support extra input types by implementing the methods  described on src/Service/Model/InputInterface and editing the config parameters to comply with the newly created InputType


