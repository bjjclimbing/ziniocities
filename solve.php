<?php
require_once  __DIR__ .('/vendor/autoload.php');
use Service\DistanceCalcService\DistanceCalcService;
use Service\InputService\InputService;
$inputService= new InputService();

$calculator = new DistanceCalcService();
$cityBundle=$inputService->getDataCities();

$route=$calculator->getRoute($cityBundle);
foreach($route as $order){
    echo $order['city']."\n";
}
