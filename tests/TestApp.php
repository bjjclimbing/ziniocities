<?php

use PHPUnit\Framework\TestCase;
use Service\DistanceCalcService\DistanceCalcService;

class DistanceCalcServiceTest extends TestCase {

    public function testDistanceCalcService() {
        $disanceCalc = new DistanceCalcService();
        $cityA = array('city' => 'Beijing', 'lat' => 39.93, 'lon' => 116.40);
        $cityB = array('city' => 'New York', 'lat' => 40.47, 'lon' => -73.58);
        $cityC = array('city' => 'Vladivostok', 'lat' => 43.8, 'lon' => 131.54);
        $expected = array($cityA, $cityC, $cityB);
        $cityBundle = array($cityA, $cityB, $cityC);
        $this->assertEquals($expected, $disanceCalc->getRoute($cityBundle));
    }
    
    public function testDistanceCalcServiceNoList() {
        $this->expectExceptionCode(906);
        $disanceCalc = new DistanceCalcService();
        $disanceCalc->getRoute(null);
    }

   public function testInputInterfaceNofile() {
        $MockedFalseparameters = array('fileSeparator' => "/[\t]/", 'fileName' => 'noexists', 'baseCity' => 'Beijing', 'itemsPerLines' => 3);
        $inputClass = new Service\InputService\Model\InputFile($MockedFalseparameters);
        $this->expectExceptionCode(901);
        $inputClass->returnData();
    }

    public function testInputInterface() {
        $MockedRealparameters = array('fileSeparator' => "/[\t]/", 'fileName' => __DIR__ . "/../cities.txt", 'baseCity' => 'Beijing', 'itemsPerLines' => 3);
        $linecount = 0;
        $handler = fopen(__DIR__ . "/../cities.txt", "r");
        while (!feof($handler)) {
            $line = fgets($handler);
            $linecount++;
        }
        fclose($handler);
        $inputClass = new Service\InputService\Model\InputFile($MockedRealparameters);
        $data = $inputClass->returnData();
        $this->assertTrue(is_array($data));
        $this->assertEquals($linecount, \count($data));
    }

    public function testInputInterfaceBadCity() {
        $MockedBadCityparameters = array('fileSeparator' => "/[\t]/", 'fileName' => __DIR__ . "/../cities.txt", 'baseCity' => 'Shambala', 'itemsPerLines' => 3);
        $inputClass = new Service\InputService\Model\InputFile($MockedBadCityparameters);
        $this->expectExceptionCode(904);
        $inputClass->returnData();
    }

    public function testInputInterfaceBadSeparator() {
        $MockedBadSeparatorparameters = array('fileSeparator' => " ", 'fileName' => __DIR__ . "/../cities.txt", 'baseCity' => 'Beijing', 'itemsPerLines' => 3);
        $inputClass = new Service\InputService\Model\InputFile($MockedBadSeparatorparameters);
        $this->expectExceptionCode(905);
        $inputClass->returnData();
    }

}
